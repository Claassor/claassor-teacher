import React from 'react';
import { SafeAreaView, StyleSheet, View, Text, TextInput , TouchableWithoutFeedback} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ArchiveItem from '../components/archiveItem'



export default function archiveScreen(){
return(
    <SafeAreaView style={styles.container}>
        
        <Text style={styles.h1} >
            اضافه کردن فایل جدید
        </Text>
        <ScrollView style={styles.flex} contentContainerStyle={{        alignItems: 'center',
        justifyContent: 'center'}}>
                <TextInput 
                placeholder='موضوع خبر'
                onChangeText={(text)=>setNotifiTitle(text)}
                placeholderTextColor="gray"
                style={styles.textInput}
                textContentType="username"
                />

                <TextInput 
                placeholder='موضوع خبر'
                onChangeText={(text)=>setNotifiTitle(text)}
                placeholderTextColor="gray"
                style={styles.textInput}
                />
                

                <TouchableWithoutFeedback  style={styles.button}>
                    <Text style={styles.btnText}>ثبت</Text>
                </TouchableWithoutFeedback>

        </ScrollView>
        <Text style={styles.h1}>
            آرشیو کلاس های شما
        </Text>
        <ScrollView style={styles.flex}>
        <ArchiveItem />
        <ArchiveItem />
        <ArchiveItem />

        </ScrollView>
    </SafeAreaView>
)
};



const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F7F9FB',
        padding: 20,
        flex: 1
    },
    h1:{
        fontSize: 20,
        fontFamily: 'kalameh-bold',
        marginBottom: 10
    },
    h2:{
        fontSize: 18,
        margin: 20
    },
    h3:{
        fontSize: 16,
        margin: 20
    },
    p:{
        fontSize: 14
    },
    flex:{
        flex: 1,
    },
    textInput: {
        fontSize: 10,
        width: 150,
        height: 30,
        padding: 8,
        borderRadius: 5,
        color: "#000000",
        borderColor: '#5842F4',
        borderWidth: 3,
        margin: 5,
      },
      btnText:{
        fontFamily: 'dana-regular'
      },
      button:{
        borderRadius: 5,
        backgroundColor: '#5842F4',
        paddingVertical: 5,
        paddingHorizontal: 12,
      }
})