import React from 'react';
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import {createStackNavigator} from '@react-navigation/stack'
import ClassComponent from '../components/classComponent'
import HomeNotifi from '../components/homeNotifi'
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { AppLoading } from 'expo';
import { useFonts } from 'expo-font';



export default function homeScreen(props){
    // let [fontsLoaded] = useFonts({
    //     'kalameh-bold': require('../../assets/fonts/Kalameh_Bold.ttf'),
    //     'dana-regular': require('../../assets/fonts/Dana-Regular.ttf'),
    //     'dana-bold': require('../../assets/fonts/Dana-Bold.ttf'), });
    //     if (!fontsLoaded) {
    //         return <AppLoading />;
    //       } else {
            return(
                <ScrollView style={styles.container}>
                <SafeAreaView >
                    <TouchableWithoutFeedback style={styles.userInfo} onPress={()=>{props.navigation.navigate('profile')}}>
                        <View style={styles.userTextContainer} >
                        <Text style={styles.h2}>خوش آمدید</Text>
                    <Text style={styles.p}>رضا سلطان محمدی</Text> 
                    <Text style={styles.p}>معلم شیمی دوازدهم</Text>
                    </View> 
                    <Image source={require('../../assets/Hajitoon.jpg')} style={styles.profilePic} />
                    </TouchableWithoutFeedback>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <ClassComponent />
                    <ClassComponent />
                    <ClassComponent />
                    <ClassComponent />
                    </ScrollView>
                    <Text style={styles.h2}>
                    کلاس های امروز شما
                    </Text>
                <HomeNotifi navigation={props.navigation} context='test prop' subject='test prop'/>
                <HomeNotifi navigation={props.navigation} context='متن خبر' subject='موضوع خبر'/>
                </SafeAreaView>
                </ScrollView>
            )
        //   }
        };
    



const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F7F9FB',
        padding: 20,
        flex: 1
    },
    profilePic:{
        width: 80,
        height: 80,
        borderRadius: 10,
        marginLeft: 10
    },
    userInfo:{
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        marginBottom: 10,
        padding: 15,
        elevation: 3,
        borderRadius: 20,
        
    },
    userData:{
        fontSize: 16,
        color: '#000000',
        fontFamily: 'dana-regular'
    },
    h1:{
        fontSize: 20
    },
    h2:{
        fontSize: 18,
        fontFamily: 'kalameh-bold',
        color: '#000000',
    },
    h3:{
        fontSize: 16,
        margin: 20
    },
    p:{
        fontSize: 14,
        color: '#000000',
        fontFamily: 'dana-regular'
    }
})

