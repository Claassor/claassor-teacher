import React, {useState} from "react";
import { View, Text, TextInput, Button, SafeAreaView , StyleSheet } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
// import BtmLabel from "../components/btmlabel";
export default  function logIn({ navigation }) {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  function loginFunc() {
    const students = Parse.Object.extend("students");
    const Query = new students();
    Query.set("username", userName);
    Query.set("Password", password);
    Query.find().then((results) => {});
    // Pass the username and password to logIn function
    // .Parse.User.logIn(userName, password)
    //   .then((user) => {
    //     // Do stuff after successful login
    //     if (typeof document !== "undefined")
    //       console.log(`Logged in user: ${JSON.stringify(user)}`);
    //     console.log("Logged in user", user);
    //     navigation.navigate("home");
    //   })
    //   .catch((error) => {
    //     alert("نام کاربری یا رمز عبور اشتباه است");
    //   });
  }
  return (
    <SafeAreaView style={styles.logInContainer}>
      <View style={styles.logIn}>
          <View style={styles.textContainer}>
        <Text style={styles.h1}>خوش آمدید</Text>
        <Text style={styles.sp}>دانش آموز عزیز
 لطفا برای ورود کدملی و رمز عبور خود را وارد کنید</Text>
 </View>
        <TextInput
          style={styles.TextInput}
          textContentType="username"
          placeholder="Enter Username"
          placeholderTextColor="gray"
          onChangeText={(text) => setUserName(text)}
        />
        <TextInput
          style={styles.TextInput}
          textContentType="password"
          placeholder="Enter password"
          placeholderTextColor="gray"
          onChangeText={(text) => setPassword(text)}
        />
        <TouchableWithoutFeedback
          style={styles.btn}
          onPress={loginFunc}
        >
          <Text style={styles.btnText}>ورود</Text>
          </TouchableWithoutFeedback>
      </View>
      <View style={styles.container}>
      <Text style={styles.header}> درباره ما</Text>
      <Text style={styles.text}>
        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده
        از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و
        سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای
        متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه
        درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با
        نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان
        خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید
        داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان
        رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات
        پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
      </Text>
    </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  logInContainer: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  logIn: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    flexDirection: "column",
  },
  TextInput: {
    fontSize: 10,
    width: 150,
    height: 30,
    padding: 8,
    borderRadius: 5,
    color: "#000000",
    borderColor: '#5842F4',
    borderWidth: 3,
    margin: 5,
  },
  h1: {
    fontSize: 20,
    fontFamily: 'dana-bold'
  },
  h1Right: {
    textAlign: "right",
    alignSelf: "stretch",
    fontSize: 20,
    marginRight: "10%",
  },
  sp: {
    textAlign: "right",
    fontSize: 14,
    color: "#8E8E8E",
    fontFamily: 'dana-regular'
  },
  btn: {
    borderRadius: 5,
    backgroundColor: '#5842F4',
    paddingVertical: 5,
    paddingHorizontal: 12
  },
  btnText:{
    fontFamily: 'dana-regular'
  },
    container: {
      alignItems: "center",
      backgroundColor: "#ffffff",
      justifyContent: "flex-end",
      width: "80%",
      height: "50%",
borderRadius:20,
      padding: 30,
      margin: 10,
      position: "relative",
      elevation: 3,
      borderColor: '#5842F4',
      borderWidth: 0.5
    },
    text: {
      overflow: "scroll",
      width: 250,
      height: "85%",
      justifyContent: "center",
      alignItems: "center",
      fontFamily: 'dana-regular'
    },
    header: {
      textAlign: "right",
      fontSize: 20,
      color: "#5842F4",
      fontFamily: 'dana-bold',
  
    },
    textContainer:{
        width: '80%'

    }
  });

