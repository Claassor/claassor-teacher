import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { ScrollView, TextInput, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import ArchiveItem from '../components/archiveItem';



export default function notifiScreen(){
    const [notifiTitle , setNotifiTitle]= useState('')
    const [notifiContext, setNotifiContext]= useState('')
    return(
        <SafeAreaView style={styles.container} >
            <Text style={styles.h1} >ثبت اعلان جدید</Text>
            <ScrollView style={styles.flex}>
            <View style={styles.notificationContainer} >
                <TextInput 
                placeholder='موضوع خبر'
                onChangeText={(text)=>setNotifiTitle(text)}
                placeholderTextColor="gray"
                style={styles.textInput}
                textContentType="username"
                />

                <TextInput 
                placeholder='موضوع خبر'
                onChangeText={(text)=>setNotifiTitle(text)}
                placeholderTextColor="gray"
                style={styles.textInput}
                />
                

                <TouchableWithoutFeedback style={styles.btn} >
                    <Text style={styles.btnText}>ثبت</Text>
                </TouchableWithoutFeedback>
                
            </View>
            </ScrollView>
            <Text style={styles.h1}>اعلانات شما</Text>
            <ScrollView style={styles.flex}>
                <Text>SAlamaLmas</Text>
                <Text>SAlamaLmas</Text>

                <Text>SAlamaLmas</Text>

                <Text>SAlamaLmas</Text>

                <Text>SAlamaLmas</Text>

                <Text>SAlamaLmas</Text>


                <Text>SAlamaLmas</Text>

                <Text>SAlamaLmas</Text>
                <Text>SAlamaLmas</Text>
                <Text>SAlamaLmas</Text>

            </ScrollView>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F7F9FB',
        padding: 20,
        flex: 1
    },
    h1:{
        fontSize: 20,
        fontFamily: 'kalameh-bold'
    },
    h2:{
        fontSize: 18,
    },
    h3:{
        fontSize: 16,
        fontFamily:'dana-bold'
    },
    p:{
        fontSize: 14,
        color: '#8e8e8e',
        fontFamily: 'dana-regular'
    },
    textInput: {
        fontSize: 10,
        width: 150,
        height: 30,
        padding: 8,
        borderRadius: 5,
        color: "#000000",
        borderColor: '#5842F4',
        borderWidth: 3,
        margin: 5,
      },
      notificationContainer:{
        alignItems: 'center',
        justifyContent: 'center'
      },
      btn: {
        borderRadius: 5,
        backgroundColor: '#5842F4',
        paddingVertical: 5,
        paddingHorizontal: 12
      },
      btnText:{
        fontFamily: 'dana-regular'
      },
      flex:{
          flex: 1
      }
       
})