import React from 'react';
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';



export default function profileScreen(){
    return(
        <SafeAreaView style={styles.container} >

            <View style={styles.userInfo} >
                <Image source={require("../../assets/Hajitoon.jpg")} style={styles.userPic}/>
                <Text style={styles.h1}>رضا سلطان محمدی</Text>
            </View>
            <View style={styles.paramsBox} >
            <Image source={require('../../assets/dayerehNesf2.png')} style={styles.dayereh1} />
            <Image source={require('../../assets/dayerehNesf2.png')} style={styles.dayereh2} />
            {/* <Image source={require('../../assets/dayerehNesf2.png')} style={styles.dayereh3} /> */}
            <View style={styles.values} >
                <Text style={styles.p} >0000</Text>
            </View>
            <View style={styles.keys} >
            <Text style={styles.h3} >adad</Text>
            <Text style={styles.h3} >adad</Text>
            <Text style={styles.h3} >adad</Text>
            <Text style={styles.h3} >adad</Text>

            </View>
            </View>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F7F9FB',
        padding: 20,
        flex: 1
    },userInfo:{
        alignSelf: 'center',
        padding: 10,
        flexDirection: 'column',
        alignItems: 'center',
    },
    userPic:{
        width: 90 ,
        height: 90,
        borderRadius: 45
    },
    paramsBox:{
        width: '90%',
        height: 200,
        backgroundColor: '#5842F4',
        borderRadius: 20,
        alignSelf: 'center',
        flexDirection: 'row',
        padding: 20,
        overflow: 'hidden'
    },
    keys:{
        flex:1,
        alignItems: 'center',
        
    },
    values:{
        flex: 1,
        alignItems: 'center'
    },
    h1:{
        fontSize: 20,
        fontFamily: 'kalameh-bold'
    },
    h2:{
        fontSize: 18,
    },
    h3:{
        fontSize: 16,
        fontFamily:'dana-bold'
    },
    p:{
        fontSize: 14,
        color: '#ffffff',
        fontFamily: 'dana-regular'
    },
    dayereh1:{
        width: 120,
        height: 120,
        position: 'absolute',
        transform: [{rotate: '137deg'}],
        top: -50,
        right: '35%'
    },
    dayereh2:{
        width: 80,
        height: 80,
        position: 'absolute',
        bottom: -8
    },
    dayereh3:{
        width: 120,
        height: 120,
        position: 'absolute',
        transform: [{rotate: '-43deg'}],
        bottom: -10,
        right: '35%'
    }

})