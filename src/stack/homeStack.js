import React from 'react';
import {  } from 'react-native';
import {createStackNavigator} from '@react-navigation/stack'
import homeScreen from '../screens/homeScreen';
import archiveScreen from '../screens/archiveScreen';
import notifiScreen from '../screens/notificationScreen';
import profileScreen from '../screens/profileScreen';

const Stack = createStackNavigator()


export default function homeStack(props){
    return (
        <Stack.Navigator>
    <Stack.Screen navigation={props.navigation} name="home" component={homeScreen}/>
    <Stack.Screen navigation={props.navigation} name="archive" component={archiveScreen}/>
    <Stack.Screen navigation={props.navigation} name="notifi" component={notifiScreen}/>
    <Stack.Screen navigation={props.navigation} name="profile" component={profileScreen}/>

</Stack.Navigator>
    )
};