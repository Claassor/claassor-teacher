import React from 'react';
import { } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import homeStack from './homeStack'
import archiveStack from './archiveStack';
import notifiScreen from '../screens/notificationScreen';
import profileScreen from '../screens/profileScreen';


const Tab = createBottomTabNavigator()

export default function tabStack(props){
    return (
    <Tab.Navigator>
        <Tab.Screen navigation={props.navigation} name="home" component={homeStack}/>
        <Tab.Screen navigation={props.navigation} name="archiveMain" component={archiveStack}/>
        <Tab.Screen navigation={props.navigation} name="notifiScreen" component={notifiScreen}/>
        <Tab.Screen navigation={props.navigation} name="profileScreen" component={profileScreen}/>

    </Tab.Navigator>
    )
};