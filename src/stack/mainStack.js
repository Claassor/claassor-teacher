import React from 'react';
import { Image } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import logInScreen from '../screens/logInScreen'
import tabStack from '../stack/tabStack'
import registerScreen from '../screens/registerScreen'
const Stack = createStackNavigator()
export default function mainStack({navigation}){
    return(
<Stack.Navigator initialRouteName="Register"  screenOptions={{headerTintColor: '#000000', headerTitleAlign: 'center',headerLeft: ()=><Image style={{width: 35 , height: 35}} source={require('../../assets/claassorLogo.png')} />, headerRight: ()=><Image style={{width: 55 , height: 55}} source={require('../../assets/ghabilehLogo.png')} />}}>
    <Stack.Screen name="Login" component={logInScreen} />
    <Stack.Screen navigation={navigation} name="Register" component={registerScreen} />
    <Stack.Screen navigation={navigation} name="Claassor"  component={tabStack} />

</Stack.Navigator>)
};