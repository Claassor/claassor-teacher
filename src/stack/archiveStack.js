import React from 'react';
import {  } from 'react-native';
import {createStackNavigator} from '@react-navigation/stack'
import archiveScreen from '../screens/archiveScreen';


const Stack = createStackNavigator()
export default function(){
    return(
        <Stack.Navigator>
        <Stack.Screen name='archiveMain' component={archiveScreen} />
        </Stack.Navigator>
        
    )
};