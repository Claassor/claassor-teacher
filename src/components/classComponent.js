import React from 'react';
import {Image, StyleSheet, Text, View  } from 'react-native';


export default function classComponent(){
    return(
        <View style={styles.classComponent}>
            <Image source={require('../../assets/dayerehNesf2.png')} style={styles.dayereh1} />
            <View style={styles.schoolInfo}>
            <Text style={styles.schoolName} >مدرسه ی شهید سلطانی</Text>
            <Image source={require('../../assets/Hajitoon.jpg')} style={styles.classImage} />
            </View>
            <View style={styles.mainCont} >
                <Text style={styles.time} >
                    00:00 - 00:00
                </Text>
                <Text style={styles.label} >
11 ریاضی</Text>
            </View>
            <Image source={require('../../assets/dayerehNesf2.png')} style={styles.dayereh2}  />
        </View>
    )
};




const styles= StyleSheet.create({
    classComponent:{
        width: 150, 
        height: 200,
        backgroundColor: '#5842F4',
        padding: 10,
        borderRadius: 20,
        overflow: 'hidden',
        margin: 8,
    }, 
    classImage:{
        width: 35,
        height: 35,
        borderRadius: 10,
        margin: 5
    },
    schoolInfo:{
        justifyContent:'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
        
    },
    schoolName:{
        fontSize: 18,
        color: '#ffffff',
        fontFamily: 'kalameh-bold',
        textAlign: 'center',
        width: 80
    },
    dayereh1:{
        position:'absolute',
        width: 100,
        height: 100,
        right: 20,
        top:-40 ,
        transform: [{ rotate: '135deg' }]  

    },
    dayereh2:{
        position:'absolute',
        width: 100,
        height: 100,
        bottom:-10
    },
    mainCont:{
        alignSelf: 'center',
        alignItems: 'center',
        padding: 20
    },
    time:{
         color: '#ffffff',
         fontFamily: 'dana-regular',
          fontSize: 14
    },
    label:{
        color: '#ffffff',
        fontFamily: 'dana-regular',
        fontSize: 14,
        alignSelf: 'center'
    }
})