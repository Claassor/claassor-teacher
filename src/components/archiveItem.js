import React from 'react';
import { Image, StyleSheet, View , Text} from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';



export default function ArchiveItem(){
    return(
        <TouchableWithoutFeedback style={styles.container}>
            <View style={styles.icons} >
                <Image source={require('../../assets/file.png')} style={styles.icon} />
                <Image source={require('../../assets/video.png')} style={styles.icon} />
            </View>
            <View style={styles.info}>
                <Text style={styles.h3}>حسابان</Text>
                <Text style={styles.p}>استاد دهدار</Text>

            </View>

        </TouchableWithoutFeedback>
    )
};



const styles = StyleSheet.create({
    container:{
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        width: '90%',
        height: 80,
        padding: 15,
        alignItems: 'center',
        borderRadius: 20,
        alignSelf: 'center',
        marginVertical: 8,
        elevation: 3
    },
    info:{
        flex: 1 ,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    icons:{
        flex: 1,
        flexDirection: 'row'
    },
    icon:{
        width:30,
        height: 30
    },  
    h3:{
        fontSize: 16,
        fontFamily: 'dana-bold',
    },
    p:{
        fontSize: 14,
        fontFamily: 'dana-regular'
    }

})