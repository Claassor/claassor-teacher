import React, { useState } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';




export default function homeNotifi(props){
    const [context , setContext] = useState(props.context);
    const [subject , setSubject] = useState (props.subject);
    return(
        <TouchableWithoutFeedback onPress={()=> {props.navigation.navigate('notifi')}}>
        <View style={styles.notifiBox}>
            <View style={styles.notifiInfo}>
                <Text style={styles.h3}>{props.context}</Text>
    <Text style={styles.p}>{props.subject}</Text>
            </View>
            <Image source={require('../../assets/favicon.png')} style={styles.icon} />
        </View>
        </TouchableWithoutFeedback>

    )
};



const styles = StyleSheet.create({
    notifiBox:{
        width: '90%', 
        height: 80,
        backgroundColor: '#ffffff',
        borderRadius: 20,
        elevation: 3,
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        padding: 15,
        alignItems: 'center'
    },
    icon:{
        width:40,
        height:40,
         borderRadius: 25,
         borderColor: '#5842F4',
         borderWidth: 1,
    },
    notifiInfo:{
        marginHorizontal: 10,
    },
    h1:{
        fontSize: 20
    },
    h2:{
        fontSize: 18,
    },
    h3:{
        fontSize: 16,
        fontFamily:'dana-bold'
    },
    p:{
        fontSize: 14,
        color: '#8e8e8e',
        fontFamily: 'dana-regular'
    }
})