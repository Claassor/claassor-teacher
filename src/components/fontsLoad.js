import React from 'react';
import {  } from 'react-native';


export default function fontsLoad(){
    let [fontsLoaded] = useFonts({
        'kalameh-bold': require('../../assets/fonts/Kalameh_Bold.ttf'),
        'dana-regular': require('../../assets/fonts/Dana-Regular.ttf'), });
        if (!fontsLoaded) {
            return <AppLoading />;
          }

};