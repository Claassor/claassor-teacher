import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import MainStack from './src/stack/mainStack'
import { AppLoading } from 'expo';
import { useFonts } from 'expo-font';
export default function App() {
  let [fontsLoaded] = useFonts({
    'kalameh-bold': require('./assets/fonts/Kalameh_Bold.ttf'),
    'dana-regular': require('./assets/fonts/Dana-Regular.ttf'),
    'dana-bold': require('./assets/fonts/Dana-Bold.ttf'), });
    if (!fontsLoaded) {
        return <AppLoading />;
      } else {
      
  return (
      <NavigationContainer>

        <MainStack />

      </NavigationContainer>
  
    )
      }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
